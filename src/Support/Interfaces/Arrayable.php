<?php

namespace Itwmw\Validation\Support\Interfaces;

interface Arrayable
{
    /**
     * 获取数组形式的实例
     *
     * @return array
     */
    public function toArray(): array;
}
