<?php

namespace Itwmw\Validation\Support\Interfaces;

interface Jsonable
{
    /**
     * 将对象转换为JSON表示
     *
     * @param  int  $options
     * @return string
     */
    public function toJson(int $options = 0): string;
}
