<?php

namespace Itwmw\Validation\Support\Concerns;

use Closure;
use Itwmw\Validation\Support\Arr;
use Itwmw\Validation\Support\Str;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Itwmw\Validation\Validator;

trait FormatsMessages
{
    use ReplacesAttributes;

    /**
     * 获取一个属性和规则的验证信息。
     *
     * @param  string  $attribute
     * @param  string  $rule
     * @return string
     */
    protected function getMessage($attribute, $rule)
    {
        $inlineMessage = $this->getInlineMessage($attribute, $rule);

        // 首先，我们将检索验证规则的自定义信息（如果存在）。
        // 如果使用的是自定义验证信息，我们将返回自定义信息，
        // 否则我们将继续搜索一个有效的信息。

        if (! is_null($inlineMessage)) {
            return $inlineMessage;
        }

        $lowerRule = Str::snake($rule);

        $customMessage = $this->getCustomMessageFromTranslator(
            $customKey = "validation.custom.{$attribute}.{$lowerRule}"
        );
        
        // 首先，我们检查属性和规则的自定义定义的验证信息。
        // 这允许开发者只为一些需要得到特别形成的属性和规则指定特定的消息。
        if ($customMessage !== $customKey) {
            return $customMessage;
        }

        // 如果被验证的规则是一个 "大小 "规则，我们将需要收集被验证的属性类型的具体错误信息，
        // 如数字、文件或字符串，它们都有不同的信息类型。
        elseif (in_array($rule, $this->sizeRules)) {
            return $this->getSizeMessage($attribute, $rule);
        }

        // 最后，如果没有设置开发者指定的信息，也没有其他特殊的信息适用于这个规则，
        // 我们将直接从翻译器服务中提取默认的信息用于这个验证规则。
        $key = "validation.{$lowerRule}";

        if ($key !== ($value = $this->translator->get($key))) {
            return $value;
        }

        return $this->getFromLocalArray(
            $attribute,
            $lowerRule,
            $this->fallbackMessages
        ) ?: $key;
    }

    /**
     * 为标准和尺寸规则获取适当的内联错误信息。
     *
     * @param  string  $attribute
     * @param  string  $rule
     * @return string|null
     */
    protected function getInlineMessage($attribute, $rule): ?string
    {
        $inlineEntry = $this->getFromLocalArray($attribute, Str::snake($rule));

        return is_array($inlineEntry) && in_array($rule, $this->sizeRules)
                    ? $inlineEntry[$this->getAttributeType($attribute)]
                    : $inlineEntry;
    }

    /**
     * 如果一个规则存在的话，获取该规则的内联信息。
     *
     * @param  string  $attribute
     * @param  string  $lowerRule
     * @param  array|null  $source
     * @return string|null
     */
    protected function getFromLocalArray($attribute, $lowerRule, $source = null)
    {
        $source = $source ?: $this->customMessages;

        $keys = ["{$attribute}.{$lowerRule}", $lowerRule];

        // 首先，我们将检查字段的特定属性规则信息的自定义信息，
        // 然后我们将检查一个非特定属性的一般自定义行。
        // 如果我们找到了其中之一，我们将返回它。
        foreach ($keys as $key) {
            foreach (array_keys($source) as $sourceKey) {
                if (false !== strpos($sourceKey, '*')) {
                    $pattern = str_replace('\*', '([^.]*)', preg_quote($sourceKey, '#'));

                    if (1 === preg_match('#^' . $pattern . '\z#u', $key)) {
                        return $source[$sourceKey];
                    }

                    continue;
                }

                if (Str::is($sourceKey, $key)) {
                    return $source[$sourceKey];
                }
            }
        }
    }

    /**
     * 从翻译器中获取自定义错误信息。
     *
     * @param  string  $key
     * @return string
     */
    protected function getCustomMessageFromTranslator($key): string
    {
        if (($message = $this->translator->get($key)) !== $key) {
            return $message;
        }

        // 如果没有找到完全匹配的键，我们将折叠所有这些信息，并在它们之间循环，
        // 试图找到一个通配符来匹配给定的键。
        // 否则，我们将简单地把键的值返回出来。
        $shortKey = preg_replace(
            '/^validation\.custom\./',
            '',
            $key
        );

        return $this->getWildcardCustomMessages(Arr::dot(
            (array) $this->translator->get('validation.custom')
        ), $shortKey, $key);
    }

    /**
     * 检查给定的信息是否有通配符密钥。
     *
     * @param  array  $messages
     * @param  string  $search
     * @param  string  $default
     * @return string
     */
    protected function getWildcardCustomMessages($messages, $search, $default)
    {
        foreach ($messages as $key => $message) {
            if ($search === $key || (Str::contains($key, ['*']) && Str::is($key, $search))) {
                return $message;
            }
        }

        return $default;
    }

    /**
     * 为一个属性和尺寸规则获取适当的错误信息。
     *
     * @param  string  $attribute
     * @param  string  $rule
     * @return string
     */
    protected function getSizeMessage($attribute, $rule)
    {
        $lowerRule = Str::snake($rule);

        // There are three different types of size validations. The attribute may be
        // either a number, file, or string so we will check a few things to know
        // which type of value it is and return the correct line for that type.
        // 有三种不同类型的尺寸验证。
        // 属性可以是数字、文件或字符串，所以我们将检查一些东西，
        // 以知道它是哪种类型的值，并为该类型返回正确的行。
        $type = $this->getAttributeType($attribute);

        $key = "validation.{$lowerRule}.{$type}";

        return $this->translator->get($key);
    }

    /**
     * 获取给定属性的数据类型。
     *
     * @param  string  $attribute
     * @return string
     */
    protected function getAttributeType($attribute)
    {
        // We assume that the attributes present in the file array are files so that
        // means that if the attribute does not have a numeric rule and the files
        // list doesn't have it we'll just consider it a string by elimination.
        // 我们假设存在于文件数组中的属性是文件，
        // 所以这意味着如果属性没有数字规则，而文件列表中也没有，
        // 我们就通过排除法将其视为一个字符串。
        if ($this->hasRule($attribute, $this->numericRules)) {
            return 'numeric';
        } elseif ($this->hasRule($attribute, ['Array'])) {
            return 'array';
        } elseif ($this->getValue($attribute) instanceof UploadedFile) {
            return 'file';
        }

        return 'string';
    }

    /**
     * 用实际值替换所有错误信息占位符。
     *
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array  $parameters
     * @return string
     */
    public function makeReplacements($message, $attribute, $rule, $parameters)
    {
        $message = $this->replaceInputPlaceholder($message, $attribute);

        if (isset($this->replacers[Str::snake($rule)])) {
            $message = $this->callReplacer($message, $attribute, Str::snake($rule), $parameters, $this);
        } elseif (method_exists($this, $replacer = "replace{$rule}")) {
            $message = $this->$replacer($message, $attribute, $rule, $parameters);
        }

        return $this->replaceAttributePlaceholder(
            $message,
            $this->getDisplayableAttribute($attribute)
        );
    }

    /**
     * 获取该属性的可显示名称。
     *
     * @param  string  $attribute
     * @return string
     */
    public function getDisplayableAttribute($attribute)
    {
        $primaryAttribute = $this->getPrimaryAttribute($attribute);

        $expectedAttributes = $attribute != $primaryAttribute
            ? [$attribute, $primaryAttribute] : [$attribute];

        foreach ($expectedAttributes as $name) {
            // 开发者可以动态地指定这个验证器实例上的自定义属性数组。
            // 如果该属性存在于这个数组中，它就会被使用，
            // 而不是用其他方式来拉出这个给定属性的属性名称。

            if (isset($this->customAttributes[$name])) {
                return $this->customAttributes[$name];
            }
        }

        // 当没有为属性指定语言行，并且它也是一个隐含的属性时，
        // 我们将显示原始属性的名称，
        // 并且在显示名称之前不会用任何这些替换来修改它。
        if (isset($this->implicitAttributes[$primaryAttribute])) {
            return ($formatter = $this->implicitAttributesFormatter)
                ? $formatter($attribute)
                : $attribute;
        }

        return str_replace('_', ' ', Str::snake($attribute));
    }

    /**
     * 替换给定信息中的:attribute占位符。
     *
     * @param  string  $message
     * @param  string  $value
     * @return string
     */
    protected function replaceAttributePlaceholder($message, $value)
    {
        return str_replace(
            [':attribute', ':ATTRIBUTE', ':Attribute'],
            [$value, Str::upper($value), Str::ucfirst($value)],
            $message
        );
    }

    /**
     * 替换给定信息中的:input占位符。
     *
     * @param  string  $message
     * @param  string  $attribute
     * @return string
     */
    protected function replaceInputPlaceholder($message, $attribute)
    {
        $actualValue = $this->getValue($attribute);

        if (is_scalar($actualValue) || is_null($actualValue)) {
            $message = str_replace(':input', $this->getDisplayableValue($attribute, $actualValue), $message);
        }

        return $message;
    }

    /**
     * 获取该值的可显示名称。
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return string
     */
    public function getDisplayableValue($attribute, $value)
    {
        if (isset($this->customValues[$attribute][$value])) {
            return $this->customValues[$attribute][$value];
        }

        $key = "validation.values.{$attribute}.{$value}";

        if (($line = $this->translator->get($key)) !== $key) {
            return $line;
        }

        if (is_bool($value)) {
            return $value ? 'true' : 'false';
        }

        if (is_null($value)) {
            return 'empty';
        }

        return (string) $value;
    }

    /**
     * 将一个属性数组转换为其可显示的形式。
     *
     * @param  array  $values
     * @return array
     */
    protected function getAttributeList(array $values): array
    {
        $attributes = [];
        
        // 对于列表中的每个属性，我们将简单地获得其可显示的形式，
        // 因为这在替换参数列表时很方便
        // 就像一些替换函数在格式化出验证信息时所做的那样。
        foreach ($values as $key => $value) {
            $attributes[$key] = $this->getDisplayableAttribute($value);
        }

        return $attributes;
    }

    /**
     * 调用一个自定义验证器的消息替换器。
     *
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array  $parameters
     * @param  Validator  $validator
     * @return string|null
     */
    protected function callReplacer($message, $attribute, $rule, $parameters, $validator)
    {
        $callback = $this->replacers[$rule];

        if ($callback instanceof Closure) {
            return $callback(...func_get_args());
        } elseif (is_string($callback)) {
            return $this->callClassBasedReplacer($callback, $message, $attribute, $rule, $parameters, $validator);
        }
    }

    /**
     * 调用一个基于类的验证器的消息替换器。
     *
     * @param  string  $callback
     * @param  string  $message
     * @param  string  $attribute
     * @param  string  $rule
     * @param  array  $parameters
     * @param  Validator  $validator
     * @return string
     */
    protected function callClassBasedReplacer($callback, $message, $attribute, $rule, $parameters, $validator)
    {
        [$class, $method] = Str::parseCallback($callback, 'replace');

        return (new $class)->{$method}(...array_slice(func_get_args(), 1));
    }
}
