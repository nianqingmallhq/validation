<?php

namespace Itwmw\Validation;

use Closure;
use Itwmw\Validation\Support\Interfaces\Rule as RuleContract;

class ClosureValidationRule implements RuleContract
{
    /**
     * 验证该属性的回调。
     *
     * @var Closure
     */
    public $callback;

    /**
     * 表示验证回调是否失败。
     *
     * @var bool
     */
    public $failed = false;

    /**
     * 验证的错误信息。
     *
     * @var string|null
     */
    public $message;

    /**
     * 创建一个新的基于闭合的验证规则。
     *
     * @param Closure $callback
     * @return void
     */
    public function __construct(Closure $callback)
    {
        $this->callback = $callback;
    }

    /**
     * 确定验证规则是否通过。
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes(string $attribute, $value): bool
    {
        $this->failed = false;

        $this->callback->__invoke($attribute, $value, function ($message) {
            $this->failed = true;

            $this->message = $message;
        });

        return ! $this->failed;
    }

    /**
     * 获取验证的错误信息。
     *
     * @return string
     */
    public function message(): ?string
    {
        return $this->message;
    }
}
