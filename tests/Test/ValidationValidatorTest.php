<?php

namespace Itwmw\Validation\Tests\Test;

use Carbon\Carbon;
use Itwmw\Validation\Support\ValidationException;
use Itwmw\Validation\Tests\Bin\BaseTest;

class ValidationValidatorTest extends BaseTest
{
    protected function tearDown(): void
    {
        Carbon::setTestNow();
    }

    public function testNestedErrorMessagesAreRetrievedFromLocalArray()
    {
        $v = $this->factory->make([
            'users' => [
                [
                    'name'  => 'Name',
                    'posts' => [
                        [
                            'name' => '',
                        ],
                    ],
                ],
            ],
        ], [
            'users.*.name'         => ['required'],
            'users.*.posts.*.name' => ['required'],
        ], [
            'users.*.name.required'         => 'user name is required',
            'users.*.posts.*.name.required' => 'post name is required',
        ]);

        $this->assertFalse($v->passes());
        $this->assertSame('post name is required', $v->error()->getMessage());
    }

    public function testSometimesWorksOnNestedArrays()
    {
        $v = $this->factory->make(['foo' => ['bar' => ['baz' => '']]], ['foo.bar.baz' => 'sometimes|required']);
        $this->assertFalse($v->passes());

        $v = $this->factory->make(['foo' => ['bar' => ['baz' => 'nonEmpty']]], ['foo.bar.baz' => 'sometimes|required']);
        $this->assertTrue($v->passes());

        $v = $this->factory->make(['foo' => ['bar', 'baz', 'moo']], ['foo' => 'sometimes|required|between:5,10']);
        $this->assertFalse($v->passes());

        $v = $this->factory->make(['foo' => ['bar', 'baz', 'moo', 'pew', 'boom']], ['foo' => 'sometimes|required|between:5,10']);
        $this->assertTrue($v->passes());
    }

    public function testValidateThrowsOnFail()
    {
        $this->expectException(ValidationException::class);

        $v = $this->factory->make(['foo' => 'bar'], ['baz' => 'required']);

        $v->validate();
    }

    public function testValidateDoesntThrowOnPass()
    {
        $v = $this->factory->make(['foo' => 'bar'], ['foo' => 'required']);

        $this->assertSame(['foo' => 'bar'], $v->validate());
    }

    public function testSometimesCanSkipRequiredRules()
    {
        $v = $this->factory->make([], ['name' => 'sometimes|required']);
        $this->assertTrue($v->passes());
    }

    public function testInValidatableRulesReturnsValid()
    {
        $v = $this->factory->make(['foo' => 'taylor'], ['name' => 'Confirmed']);
        $this->assertTrue($v->passes());
    }

    public function testValidateEmptyStringsAlwaysPasses()
    {
        $v = $this->factory->make(['x' => ''], ['x' => 'size:10|array|integer|min:5']);
        $this->assertTrue($v->passes());
    }

    public function testEmptyExistingAttributesAreValidated()
    {
        $v = $this->factory->make(['x' => ''], ['x' => 'array']);
        $this->assertTrue($v->passes());

        $v = $this->factory->make(['x' => []], ['x' => 'boolean']);
        $this->assertFalse($v->passes());

        $v = $this->factory->make(['x' => []], ['x' => 'numeric']);
        $this->assertFalse($v->passes());

        $v = $this->factory->make(['x' => []], ['x' => 'integer']);
        $this->assertFalse($v->passes());

        $v = $this->factory->make(['x' => []], ['x' => 'string']);
        $this->assertFalse($v->passes());

        $v = $this->factory->make([], ['x' => 'string', 'y' => 'numeric', 'z' => 'integer', 'a' => 'boolean', 'b' => 'array']);
        $this->assertTrue($v->passes());
    }
}
