<?php

namespace Itwmw\Validation\Tests\Bin;

use Itwmw\Validation\Factory;
use PHPUnit\Framework\TestCase;

class BaseTest extends TestCase
{
    protected $factory;

    public function __construct(?string $name = null, array $data = [], $dataName = '')
    {
        $this->factory = new Factory();
        parent::__construct($name, $data, $dataName);
    }

    public function checkRule($data, $rule, bool $pass, string $message = null, string $attribute = null)
    {
        if (!is_array($data)) {
            $data = [ 's' => $data ];
        }

        if (!is_array($rule)) {
            $rule = [ 's' => $rule];
        }
        $v      = $this->factory->make($data, $rule);
        $result = $v->passes();
        $this->assertSame($pass, $result);
        $exception = $v->error();
        if (is_null($exception)) {
            $errorMessage   = null;
            $errorAttribute = null;
        } else {
            $errorMessage   = $exception->getMessage();
            $errorAttribute = $exception->getAttribute();
        }

        if (!is_null($message)) {
            $this->assertSame($errorMessage, $message);
        }

        if (!is_null($attribute)) {
            $this->assertSame($errorAttribute, $attribute);
        }
    }
}
